golang-github-powerman-check (1.8.0-2) unstable; urgency=medium

  * Merge diverged git repos
  * Remove now unnecessary patches
  * Standards-Version to 4.7.0

 -- Eric Dorland <eric@debian.org>  Sat, 01 Feb 2025 14:18:46 -0500

golang-github-powerman-check (1.8.0-1) unstable; urgency=medium

  * New upstream release
  * Fix golang-github-powerman-deepequal-dev deps
  * Add build deps on tzdata-legacy for test suite (Closes: 1086268)

 -- Eric Dorland <eric@debian.org>  Sun, 15 Dec 2024 23:07:53 -0500

golang-github-powerman-check (1.6.0-1) UNRELEASED; urgency=medium

  * New upstream release
  * Add dep on golang-github-powerman-deepequal
  * Standards-Version to 4.6.0.1
  * Debhelper to v13

 -- Eric Dorland <eric@debian.org>  Mon, 20 Sep 2021 12:35:12 -0400

golang-github-powerman-check (1.3.0-1) UNRELEASED; urgency=medium

  [ Eric Dorland ]
  * New upstream release
  * Add deps on golang-google-grpc-dev and golang-google-protobuf-dev

 -- Eric Dorland <eric@debian.org>  Sun, 03 Jan 2021 22:34:56 -0500

golang-github-powerman-check (1.6.0-2) unstable; urgency=medium

  * Team upload.
  * Add tzdata to Build-Depends. Closes: #1027370.

 -- Santiago Vila <sanvila@debian.org>  Tue, 31 Oct 2023 23:45:00 +0100

golang-github-powerman-check (1.6.0-1) unstable; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Shengjing Zhu ]
  * New upstream version
  * Update debhelper-compat to 13
  * Update Standards-Version to 4.6.2 (no changes)
  * Update section to golang
  * Add patch to fix tests with Go1.19 (Closes: #1017302)
  * Update dependencies
    + golang-github-powerman-deepequal-dev
    + golang-google-protobuf-dev
  * Add patch to revert grpc status support

 -- Shengjing Zhu <zhsj@debian.org>  Wed, 22 Feb 2023 22:09:32 +0800

golang-github-powerman-check (1.2.1-2) unstable; urgency=medium

  * Standards-Version to 4.5.0.2

 -- Eric Dorland <eric@debian.org>  Sun, 21 Jun 2020 03:23:45 -0400

golang-github-powerman-check (1.2.1-1) unstable; urgency=medium

  * Initial release (Closes: 948233)

 -- Eric Dorland <eric@debian.org>  Fri, 03 Jan 2020 23:18:21 -0500
